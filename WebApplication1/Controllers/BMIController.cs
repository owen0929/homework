﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.ViewModel;

namespace WebApplication1.Controllers
{
    public class BMIController : Controller
    {
        // GET: BMI
        public ActionResult Index()
        {
            return View(new BMIData());
        }

        [HttpPost]
        public ActionResult Index(BMIData data)
        {
            if (ModelState.IsValid)
            {
                float m_height = data.Height / 100;
                float bmi = data.Weight / (m_height * m_height);

                

                data.BMI = bmi;
                data.Level = level;
            }
            return View(data);
        }
    }
}
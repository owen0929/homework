﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Data()
        {
            ViewData["school"] = "靜宜";
            return View();
        }
        public ActionResult Bag()
        {
            ViewBag.phone = "04-23645646";
            return View();

        }
    }
}